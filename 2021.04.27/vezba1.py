import requests
import datetime
import time

print("Testiranje portala gubitnik.com")
pocetno_vreme = time.time()
rezultat = requests.get("http://gubitnik.com").text
ocekivani_rezultat = "sa sitnim nutom"
prihvatljiv_rezultat = "sa krupnim nutom"
print("Ocekivano:",ocekivani_rezultat,"; Dobijeno:",rezultat)
krajnje_vreme = time.time()
print("Vreme testiranja",datetime.datetime.now())
print("Trajanje testa",krajnje_vreme-pocetno_vreme)

# if ocekivani_rezultat == rezultat:
#     poruka = "Pozitivan" 
# else:
#     poruka = "Negativan"

#poruka = "Pozitivan" if ocekivani_rezultat == rezultat else "Negativan"

if ocekivani_rezultat == rezultat:
    poruka = "Pozitivan"
elif prihvatljiv_rezultat == rezultat:
    poruka = "Prihvaljiv"
else:
    poruka = "Negativan"


print("Rezultat testiranja:",poruka)