import requests

utakmice = requests.get("https://sbook.promocote.com/api/live/getlivematches/1").json() 

upper_limit = 2
wrong_name  = "FK Crvena Zvezda"

for jedna_utakmica in utakmice["payload"]:
    rezultat = utakmice["payload"][jedna_utakmica]["mInfo"]["betState"]["score"]
    utakmica = utakmice["payload"][jedna_utakmica]["mInfo"]["match"]
    if rezultat["home"] > upper_limit or rezultat["away"] > upper_limit:
        print("Pogresan rezultat (",rezultat,") za mec ",utakmica["homeTeam"],":",utakmica["awayTeam"])
    if utakmica["homeTeam"] == wrong_name or utakmica["awayTeam"] == wrong_name:
        print("Neisparavan naziv tima za mec ",utakmica["homeTeam"],":",utakmica["awayTeam"])
