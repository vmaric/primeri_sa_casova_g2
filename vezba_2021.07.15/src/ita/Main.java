package ita;

public class Main {
    public static void main(String[] args) {
        Pivo p          = new Pivo();
        p.naziv         = "Kapuzziner";
        p.vrsta         = "Jecmeno";
        p.temperatura   = 48;
        int nova_temperatura = p.zagrej(2);
        int ocekivano = 50;
        boolean ok = ocekivano == nova_temperatura;
        System.out.println(ok);
    }
}
