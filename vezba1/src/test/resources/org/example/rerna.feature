Feature:
  Provera temperature i paljenja

    Scenario Outline:
      When postavljam temperaturu na <temperatura>
      Then rerna je ukljucena
      And temperatura je <temperatura>
      When ugasi
      Then rerna je iskljucena
      And temperatura je 0

      Examples:
        | temperatura |
        | 30 |
        | 40 |
        | 50 |
        | 55 |
        | 15 |
        | 25 |
        | 100 |