package org.example;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class ProveraRerne {
    Rerna r;
    @When("postavljam temperaturu na {float}")
    public void postavi_temperaturu(float t){
        r = new Rerna();
        r.postavi_temperaturu(t);
        System.out.println("Postavljamo temperaturu na " + t);
    }
    @Then("rerna je ukljucena")
    public void da_li_je_ukljucena(){
        Assert.assertTrue(r.status);
        System.out.println("Provera rada rerne");
    }
    @And("temperatura je {float}")
    public void provera_temperature(float t){
        System.out.println("Provera temperature " + t + " , " + r.temperatura);
        Assert.assertEquals(t,r.temperatura,0);
    }
    @When("ugasi")
    public void ugasi(){
        r.iskljuci_rernu();
    }
    @Then("rerna je iskljucena")
    public void provera_iskljucena(){
        Assert.assertFalse(r.status);
    }
}
