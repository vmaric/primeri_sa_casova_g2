import requests
domeni      = ["gresnik.com","gubitnik.com","jadnik.com"]
predstave   = ["Mnogo buke ni oko cega","Radovan III","Sabirni centar","Kabare Lane gutovic"] 
cene        = ["400","400","500","600"]

for domen in domeni:
    print("Testirani domen:",domen) 
    for i in range(len(predstave)):
        odgovor = requests.get(f"http://{domen}?id={i}").text 
        odgovor_delovi = odgovor.split(",") 
        if predstave[i] != odgovor_delovi[0]:
            print("Greska u naslovu",predstave[i],odgovor_delovi[0])
        if cene[i] != odgovor_delovi[1]:
            print("Greska u ceni",cene[i],odgovor_delovi[1])

print("Test je zavrsen")