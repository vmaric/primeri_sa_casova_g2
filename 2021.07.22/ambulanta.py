class Ambulanta: 
    '''
    Obradjuje sve sto se tice ambulante
    '''
    def plati(self,ko,koliko,sto):
        '''
        Vrsi uplatu na racun ambulante
        Args:
            ko: uplatilac - string
            koliko: uplaceni iznos - float
            sto: svrha uplate - string 
        '''
        print(f"{ko} placa {koliko} za {sto}")

    def profit(self):
        '''
        Prikazuje profit ambulante
        '''
        print(30000)