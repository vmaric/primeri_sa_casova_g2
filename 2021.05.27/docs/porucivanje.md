# Servis za porucivanje

REST servis pomocu koga klijenti mogu da izvrsavaju porudzbine

## Upotreba servisa

Svi zahtevi ka servisu moraju biti HTTP POST zahtevi. Telo zahteva mora sadrzati JSON ili urlencoded tekst.

### Primer JSON zahteva

	POST /poruci HTTP/1.1
	Host: porucivanjepica.com
	Content-type: application/json
	
	{"idpica":123,"kolicina":2,"companyid":5}
	
### Primer urlencoded zahteva

	POST /poruci HTTP/1.1
	Host: porucivanjepica.com
	Content-type: application/x-www-form-urlencoded
	
	idpica=123&kolicina=2&companyid=5
	
[Preuzmi fajl sa primerom](fajlovi/porudzbina.json)