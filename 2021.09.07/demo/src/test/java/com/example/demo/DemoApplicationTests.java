package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void test_sloba(){
		String ocekivano = "<h3>Sloba Kocic</h3>Kako ide QA?";
		Reziser r = new Reziser();
		Assertions.assertEquals(ocekivano,r.sloba());
	}
	@Test
	void homepage(){
		String ocekivano = "<a href='/hello'>Hello</a> <a href='/sloba'>Sloba</a> <a href='/onama'>O nama</a>";
		Reziser r = new Reziser();
		Assertions.assertEquals(ocekivano,r.homepage());
	}
	@Test
	void onama(){
		String ocekivano = "Strana o nama";
		Reziser r = new Reziser();
		Assertions.assertEquals(ocekivano,r.onama());
	}

}
