package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestZahteva {
    @LocalServerPort
    int port;

    @Autowired
    TestRestTemplate template;

    @Test
    public void dovla(){
        String odgovor = template.getForObject("http://localhost:"+port,String.class);
        Assertions.assertEquals(odgovor,"<a href='/hello'>Hello</a> <a href='/sloba'>Sloba</a> <a href='/onama'>O nama</a>");
    }

    @Test
    public void test_gresnik(){
        String odgovor = template.getForObject("http://gresnik.com/",String.class);
        System.out.println(odgovor);
    }
}
