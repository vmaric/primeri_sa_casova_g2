package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Reziser {

    @RequestMapping("/")
    public @ResponseBody
    String homepage(){
        return "<a href='/hello'>Hello</a> <a href='/sloba'>Sloba</a> <a href='/onama'>O nama</a>";
    }

    @RequestMapping("/hello")
    public @ResponseBody
    String hello(){
        return "Hello from me";
    }

    @RequestMapping("/sloba")
    public @ResponseBody String sloba(){
        return "<h3>Sloba Kocic</h3>Kako ide QA?";
    }


    @RequestMapping("/onama")
    public @ResponseBody String onama(){
        return "Strana o nama";
    }
}
