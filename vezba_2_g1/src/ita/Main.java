package ita;

import javax.swing.*;
import java.awt.*;

public class Main {

    /**
     * Mora da se uci i vezba, Dragana rekla
     * @param args
     */
    public static void main(String[] args) {

        JFrame jf = new JFrame("Hello");
        jf.setPreferredSize(new Dimension(800,600));

        JButton taster = new JButton("Klikni me");
        taster.addActionListener(e -> {
            System.out.println("Klikn'o si me!!!!");
        });
        jf.getContentPane().add(taster);

        jf.setSize(800,600);
        jf.setVisible(true);
//
//        Predavac dovla  = new Predavac();
//        dovla.ime       = "Dovla";
//        dovla.predmet   = "Kvalitet Softvera";
//        dovla.objasni();
//        dovla.napravi_pauzu();
//        Predavac veselin = new Predavac();

//          Predavac p = new Predavac();
//          String objasnjeno = p.objasni();
//          String ocekivano = "Dovla objasnjava javu";
//          System.out.println(objasnjeno == ocekivano);

    }
}
