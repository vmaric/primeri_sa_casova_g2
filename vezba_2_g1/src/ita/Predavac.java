package ita;

/***
 * Dovla je car
 *
 * @param x ime
 */
public class Predavac {
    String ime;
    String predmet;

    /**
     * Mora da se uci i da se vezba
     */
    public String objasni(){
        System.out.println(ime+" objasnjava "+predmet);
        return "Dovla objasnjava javu";
    }

    /**
     * Pravljenje pauze
     */
    public void napravi_pauzu(){
        System.out.println("Pauza pocinje, mozete napolje");
    }
}
