class Proizvod:
    def __init__(self,m,p,c):
        self.model          = m
        self.proizvodjac    = p
        self.cena           = c
    def predstavi_se(self):
        print(self.model,self.proizvodjac,self.cena,end=" ")

    
class Telefon(Proizvod):
        broj_kamera = 0
        def predstavi_se(self):
            super().predstavi_se()
            print(self.broj_kamera)

class Cekic(Proizvod):
        vrsta_rucke = ""
        def predstavi_se(self):
            super().predstavi_se()
            print(self.vrsta_rucke)

