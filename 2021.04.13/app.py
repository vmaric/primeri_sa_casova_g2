import mysql.connector as conn
import http.server as server


class Handler(server.SimpleHTTPRequestHandler):
    def do_GET(self):
        db = conn.connect(username="root",database="telefoni",passwd="")
        cur = db.cursor()
        cur.execute("select * from telefoni")
        res = ""
        svi = cur.fetchall()
        for tel in svi:
            res+=f"<div style='border:1px solid red;padding:4px;margin:4px;'>{tel[1]} {tel[2]} cena: {tel[3]}</div>" 
        self.send_response(200)
        self.send_header("Content-type","text/html")
        self.end_headers()
        self.wfile.write(res.encode())


server.HTTPServer(("0.0.0.0",8000),Handler).serve_forever()