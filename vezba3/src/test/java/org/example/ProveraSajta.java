package org.example;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class ProveraSajta {

    String odgovor;
    int status;

    @When("unesem {string}")
    public void unesem(String poslato) {
        Client klijent = ClientBuilder.newClient();
        String ocekivano = "Welcome " + poslato;
        WebTarget strana = klijent.target("http://gresnik.com/?ime="+poslato);
        Response odgovor = strana.request().get();
        this.status = odgovor.getStatus();
        this.odgovor = odgovor.readEntity(String.class);
        System.out.println(this.odgovor);
    }

    @Then("dobijem {string}")
    public void dobijem(String string) {
        Assert.assertEquals(string,this.odgovor);
    }

    @And("status is {int}")
    public void status(int status){
        Assert.assertEquals(status,this.status);
    }
}
