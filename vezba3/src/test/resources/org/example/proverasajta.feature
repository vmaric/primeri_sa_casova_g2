Feature:
  Provera sajta

  Scenario Outline:
    When unesem "<poslato>"
    Then dobijem "<dobijeno>"
    And status is <status>

    Examples:
      | poslato | dobijeno | status |
      | dovla | Welcome dovla | 200 |
      | andrija | Welcome andrija | 200 |
      | sloba | Welcome sloba | 200 |
      | marija | Welcome marija | 200 |