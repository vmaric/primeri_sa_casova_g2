import mysql.connector as conn
from flask import Flask,request,jsonify
 
db = conn.connect(host="localhost",username="root",database="cipele",passwd="") 
 

app = Flask("Cipele REST API")

@app.route("/cipela/<idcipele>",methods=["GET","DELETE","PUT"])
@app.route("/cipela",methods=["GET","POST"])
def cipele(idcipele=None):  
    metod = request.method.lower()
    if metod == "put":
        if idcipele:
            telo = request.json
            naziv = telo["naziv"]
            cena = telo["cena"]
            cur = db.cursor() 
            cur.execute("update cipele set naziv=%s,cena=%s where id = %s",(naziv,cena,idcipele))  
            db.commit()
            cur.close() 
            return {"success":1} 
        else:
            return {"error":1}
    if metod == "delete":
        if idcipele:
            cur = db.cursor() 
            cur.execute("delete from cipele where id = %s",(idcipele,))  
            db.commit()
            cur.close() 
            return {"success":1}
        else:
            return {"error":1}
    elif metod == "get":
        if idcipele:
            cur = db.cursor() 
            cur.execute("select * from cipele where id = %s",(idcipele,))  
            cid,naziv,cena = cur.fetchone()
            cipele = {"id":cid,"naziv":naziv,"cena":float(cena)}
            cur.close() 
        else:
            cur = db.cursor() 
            cur.execute("select * from cipele") 
            cipele = [] 
            for cid,naziv,cena in cur.fetchall():
                cipele.append({"id":cid,"naziv":naziv})
            cur.close() 
        return jsonify(cipele)
    elif metod == "post":
        telo = request.json
        naziv = telo["naziv"]
        cena = telo["cena"]
        cur = db.cursor() 
        cur.execute("insert into cipele values (null,%s,%s)",(naziv,cena)) 
        idcipele = cur.lastrowid
        db.commit()
        cur.close() 
        return {"id":idcipele}

    

app.run(debug=True)
