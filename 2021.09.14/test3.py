import requests
import random

import time
import timeit


def test():
    for i in range(1000):
        cipela = {"naziv":f"cipela_{i}","cena":random.randint(100,200)} 
        odgovor = requests.post("http://127.0.0.1:5000/cipela",json=cipela).json()
        assert odgovor["id"] > 0


print(timeit.timeit(test,number=1))