import requests

sve_cipele = requests.get("http://127.0.0.1:5000/cipela").json()

for cipela in sve_cipele:
    odgovor = requests.delete(f"http://127.0.0.1:5000/cipela/{cipela['id']}").json()
    assert "success" in odgovor