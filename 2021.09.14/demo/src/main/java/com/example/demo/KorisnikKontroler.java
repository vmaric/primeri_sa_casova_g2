package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/korisnik")
public class KorisnikKontroler {
    @GetMapping
    public String pozdrav(){
        return "Hello";
    }

    @PostMapping("/login")
    public Object login(@RequestBody Map<String,String> podaci){
        if(podaci.get("username").equals("andrija")&&podaci.get("password").equals("123")){
            return "{\"success\":1}";
        }
        return "{\"success\":0}";
    }

}
