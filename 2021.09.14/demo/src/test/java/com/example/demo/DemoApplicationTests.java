package com.example.demo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class DemoApplicationTests {

	@Autowired
	KorisnikKontroler ctrl;

	@Autowired
	TestRestTemplate template;

	@Test
	void proveraCipela(){
		Map<String,String> odgovor = template.getForObject("http://localhost:5000/cipela/6822",Map.class);
		Assertions.assertEquals(String.valueOf(odgovor.get("cena")),"2500.0");
	}

	@Test
	void proveraHttpa(){
		Map<String,String> podaci = new HashMap<String,String>();
		podaci.put("username","andrija");
		podaci.put("password","123");
		String odgovor = template.postForObject("http://localhost:8080/korisnik/login",podaci,String.class);
		Assertions.assertEquals("{\"success\":1}",odgovor);
	}

	@Test
	void contextLoads() {
		Map<String,String> podaci = new HashMap<String,String>();
		podaci.put("username","andrija");
		podaci.put("password","123");
		Object odgovor = ctrl.login(podaci);
		Assertions.assertEquals("{\"success\":1}",odgovor.toString());
	}

}
