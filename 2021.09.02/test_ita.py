from selenium.webdriver import Chrome
from selenium.webdriver import ChromeOptions
import requests
import urllib.request as req
import time,shutil,filecmp
import threading

def provera():
    opcije = ChromeOptions()
    opcije.add_argument("--log-level=3")
    #opcije.add_argument("--headless=true")
    opcije.add_argument("--no-sandbox")
    browser = Chrome(options=opcije)
    browser.get("https://it-akademija.com")  
    browser.save_screenshot("naslovna.png") 
    link = browser.find_element_by_link_text("O IT AKADEMIJI")
    link.click()  
    browser.implicitly_wait(10) 
    browser.save_screenshot("onama.png")
    closer = browser.find_element_by_id("zatvori")
    closer.click() 
    browser.save_screenshot("predavaci.png")
    predavaci = browser.find_element_by_link_text("ITAcademy tim i predavači")
    predavaci.click() 
    tekst_za_poredjenje = "ITAcademy predavači. Ovi ljudi mogu da budu korisni za napredovanje Vaše karijere"
    tekst_na_sajtu = browser.find_element_by_tag_name("h1")
    assert tekst_na_sajtu.text == tekst_za_poredjenje 
    svi_predavaci = browser.find_elements_by_class_name("oIti") 
    assert "Vladimir Marić" in svi_predavaci[1].text 
    dovla_slika = svi_predavaci[1].find_element_by_tag_name("img")  
    r = requests.get(dovla_slika.get_attribute("src"), stream = True) 
    r.raw.decode_content = True 
    with open("slika.jpg",'wb') as f:
        shutil.copyfileobj(r.raw, f) 
    assert filecmp.cmp("referenca.jpg","slika.jpg") 

    browser.find_element_by_link_text("DL platforma").click()

    browser.find_element_by_id("username").send_keys("dovla")
    browser.find_element_by_id("password").send_keys("123")
    browser.find_element_by_id("submit").click()
    postoji = False
    for div in browser.find_elements_by_tag_name("div"):
        if "Uneli ste pogrešno korisničko ime ili šifru" in div.text:
            postoji = True
            break

    assert postoji

    #time.sleep(10)

    browser.quit()


def test_ita():
    provera()

#test_ita()
