import requests

def parselinks(sadrzaj):
    sadrzaj = sadrzaj.replace("\n","")
    sadrzaj = sadrzaj.replace("</a><a href=\"","|")
    sadrzaj = sadrzaj.lstrip("<a href=\"")
    sadrzaj = sadrzaj.replace("\">",",")
    sadrzaj = sadrzaj.rstrip("</a>")
    return sadrzaj

def parsirajkompove(txt):
    odgovor = [tuple(komp.split(",")) for komp in txt.split("|")]
    return odgovor

def preuzmilinkove(url):
    res = requests.get(url).text
    return res 

def parsetitle(html):
    #html = """<title>zx81</title>
    #ZX 81 najjaci"""
    html = html.lstrip("<title>")
    html = html.split("</title>")
    html = html[0].strip()
    return html

def proveristranu(strana,title):
    odgovor = requests.get(strana).text
    odgovor_title = parsetitle(odgovor)
    return title == odgovor_title

def testiraj(url):
    kompovi = preuzmilinkove(url)
    linkovi = parselinks(kompovi)
    kompovi_parsed = parsirajkompove(linkovi) 
    for path,komp in kompovi_parsed: 
        print(proveristranu(f"{url}/{path}",komp))


