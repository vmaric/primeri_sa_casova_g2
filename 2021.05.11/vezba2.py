import random 
broj_uspesnih_pokusaja  = 0
ukupno_testova          = 0 
while True:
    kocnice_rade = True
    ukupno_testova += 1
    for _ in range(5): 
        uspesan_test = random.randint(0,6) != 1
        if not uspesan_test:
            kocnice_rade = False
            break 
    if kocnice_rade:
        broj_uspesnih_pokusaja += 1
    else:
        broj_uspesnih_pokusaja = 0
        continue 
    if  broj_uspesnih_pokusaja > 2:
        break 
print("Testiranje je uspesno, sva vozila su OK")
print("Ukupno testova:",ukupno_testova)
