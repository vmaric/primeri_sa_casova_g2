import requests

ukupan_broj_pokusaja    = 0
ukupno_nula             = 0

for broj_testa in range(1,6):
    greska          = False
    broj_pokusaja   = 0
    nule            = 0
    print(f"Test broj {broj_testa}: ")
    while not greska:
        broj_pokusaja += 1 
        odgovor = requests.get("http://gresnik.com?pas=bulldog").text 
        print("Pokusaj broj:",broj_pokusaja,"Dobijeno:",odgovor,"          ",end="\r")
        if odgovor == "0":
            nule += 1
        if not odgovor or odgovor == 'NaN':
            print()
            greska = True 
    ukupan_broj_pokusaja += broj_pokusaja
    ukupno_nula += nule 
    print("Totalno pokusaja pre greske:",broj_pokusaja)
    print("Totalno besplatnih pasa:",nule)
print("#"*40)
print("Ukupno pokusaja:",ukupan_broj_pokusaja)
print("Ukupno besplatnih pasa",ukupno_nula)
print("Prosecan broj pokusaja pre greske",ukupan_broj_pokusaja/5)