import requests
from bs4 import BeautifulSoup

def podaci():
    linije = open("podaci.txt","r").readlines()
    linije = [tuple(linija.strip().split(",")) for linija in linije]
    return linije

def podaci_server():
    odgovor = requests.get("http://gresnik.com").text
    odgovor = BeautifulSoup(odgovor,features="html.parser")
    linkovi = odgovor.find_all("a")
    linkovi = [link for link in linkovi if "product.php" in link["href"]]
    linkovi = [(link.text.lower(),link["href"]) for link in linkovi]
    return linkovi

def cena(link):
    odgovor = requests.get(f"http://gresnik.com/{link}").text
    return odgovor.strip().split(" ")[1]

def provera_proizvoda():
    nazivi_lokal = [podatak[0] for podatak in podaci()]
    nazivi_server = [podatak[0] for podatak in podaci_server()]
    # nazivi_lokal.sort()
    # nazivi_server.sort()
    # print(nazivi_lokal,nazivi_server)
    # print(nazivi_lokal==nazivi_server)
    for naziv in nazivi_lokal:
        if naziv not in nazivi_server:
            return False
    return True 

def provera_cena():
    proizvodi_server    = podaci_server()
    proizvodi_lokal     = podaci()
    for naziv,ocekivana_cena in proizvodi_lokal:
        link = [p[1] for p in proizvodi_server if p[0] == naziv][0]
        dobijena_cena = cena(link)
        if dobijena_cena != ocekivana_cena:
            return False
    return True