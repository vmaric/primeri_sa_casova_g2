package org.example;

import org.junit.*;
import org.junit.runners.MethodSorters;

import java.util.List;

public class OdecaServisTest {

    static OdecaServis os;

    @BeforeClass
    public static void pre_svih_testova(){
        os = new OdecaServis();
        System.out.println("Startuje se pre svih testova");
    }

    @Before
    public void pre_test_metode(){
        System.out.println("Ovo se startuje pre svake test metode");
    }

    @After
    public void posle_test_metode(){
        System.out.println("Ovo se startuje posle svake test metode");
    }

    @Test
    public void test_vrati_odecu(){
        Odeca o = os.vrati_odecu(1);
        Assert.assertNotNull(o);
    }

    @Test
    public void test_vrati_cenu(){
        double ocekivano = 150;
        Odeca o = new Odeca();
        o.cena = 150;
        double dobijeno = os.vrati_cenu(o);
        Assert.assertEquals(ocekivano,dobijeno,0);
    }

    @Test
    public void test_vrati_proizvode(){
        List proizvodi = os.vrati_proizvode();
        Assert.assertNotNull(proizvodi);
    }

    @Test
    public void test_prazna_lista(){
        List proizvodi = os.vrati_proizvode();
        Assert.assertNotEquals(0,proizvodi.size());
    }

}
