package ita;
public class Main {
    static class Covek {  }
    static class Automobil {
        String ime;
        int godiste;
    }


    public static void main(String[] args) {
        Polaznik p = new Polaznik();
        p.ime = "Aleksandra";
        p.generacija = 2018;
        p.smer = "Java";
        p.say_hello();

        Polaznik v = new Polaznik();
        v.ime = "Veselin";
        v.generacija = 2021;
        v.smer = "QA";
        v.say_hello();
    }
}
