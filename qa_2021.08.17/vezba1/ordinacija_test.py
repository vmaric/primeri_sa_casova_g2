from ordinacija import cena, izracunaj_cenu, vrati_cenu

def testiraj_cenu():
    vraceno = vrati_cenu("ekg")
    ocekivano = 1000
    assert vraceno == ocekivano

def testiraj_izracunavanje():
    vraceno = izracunaj_cenu(1000)
    ocekivano = 1200
    assert vraceno == ocekivano


testiraj_cenu()
testiraj_izracunavanje()