import unittest
from ordinacija import vrati_cenu, izracunaj_cenu 

class TestOrdinacija(unittest.TestCase):
    def test_vrati_cenu(self):
        ocekivano = 1000
        dobijeno = vrati_cenu("ekg")
        self.assertEqual(ocekivano,dobijeno) 

    def test_izracunaj_cenu(self):
        ocekivano = 1200
        dobijeno = izracunaj_cenu(1000)
        self.assertEqual(ocekivano,dobijeno)
        self.assertIs(type(dobijeno),float)