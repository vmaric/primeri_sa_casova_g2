model = {
    "ekg":1010,
    "laboratorija":1300,
    "skener":3000
}

def izracunaj_cenu(suma):
    return suma * 1.2

def vrati_cenu(proizvod):
    return model[proizvod]