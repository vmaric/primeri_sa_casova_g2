from ordinacija import izracunaj_cenu, vrati_cenu
import ordinacija
import pytest

@pytest.fixture
def funkcija_koja_definise_model():
    ordinacija.model = { "ekg":1000 } 

def test_izracunaj_cenu(funkcija_koja_definise_model): 
    ocekivano = 1200
    dobijeno = izracunaj_cenu(1000)
    assert ocekivano == dobijeno

def test_vrati_cenu(funkcija_koja_definise_model): 
    ocekivano = 1000
    dobijeno = vrati_cenu("ekg")
    assert ocekivano == dobijeno

def test_proveri_sajt():
    import requests
    odgovor = requests.get("http://collabedit.com").status_code
    assert odgovor == 200
 