package org.example;

import org.junit.Assert;
import org.junit.Test;

public class DovlaTest {

    @Test
    public void testiraj_cenu(){
        double ocekivano = 1000;
        double dobijeno = new Ordinacija().vrati_cenu("ekg");
        Assert.assertEquals(ocekivano,dobijeno,0);
    }
    @Test
    public void porez(){
        double ocekivano = 1100;
        double dobijeno = new Ordinacija().izracunaj_cenu(1000);
        Assert.assertEquals(ocekivano,dobijeno,0);
    }
}
