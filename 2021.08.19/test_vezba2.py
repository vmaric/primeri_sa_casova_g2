import requests
from bs4 import BeautifulSoup

def test_naslov():
    ocekivano = "Dobro dosao Dovla"
    odgovor = requests.get("http://gresnik.com/hrana.php")
    print("Statusni kod:",odgovor.status_code)
    print("Zaglavlja:")
    for k,v in odgovor.headers.items():
        print(k,":",v)
    sadrzaj = odgovor.text 
    html = BeautifulSoup(sadrzaj,features='html.parser') 
    dobijeno = html.find("h3").text 
    assert ocekivano==dobijeno