import requests
from bs4 import BeautifulSoup

def test_naslov():
    ocekivano = "Dobro dosao Dovla"
    odgovor = requests.get("http://gresnik.com/hrana.php")
    print("Statusni kod:",odgovor.status_code)
    print("Zaglavlja:")
    for k,v in odgovor.headers.items():
        print(k,":",v)
    sadrzaj = odgovor.text 
    html = BeautifulSoup(sadrzaj,features='html.parser') 
    dobijeno = html.find("h3").text 
    assert ocekivano==dobijeno

def test_cena(): 
    prva_strana = requests.get("http://gresnik.com/hrana.php")
    html = BeautifulSoup(prva_strana.text)
    linkovi = html.find_all("a")
    for link in linkovi:
        url = "http://gresnik.com/"+link['href']
        druga_strana = requests.get(url)
        cena_i_naslov = druga_strana.text.split("Cena:")
        cena = cena_i_naslov[1].strip()
        naziv = cena_i_naslov[0].replace("<h3>","").replace("</h3>\n","")
        assert cena.isnumeric()
