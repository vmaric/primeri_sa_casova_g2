import requests
from bs4 import BeautifulSoup
import pprint

def test_cena(): 
    prva_strana = requests.get("http://gresnik.com/hrana.php")
    html = BeautifulSoup(prva_strana.text)
    linkovi = html.find_all("a")
    for link in linkovi:
        url = "http://gresnik.com/"+link['href']
        druga_strana = requests.get(url)
        cena_i_naslov = druga_strana.text.split("Cena:")
        cena = cena_i_naslov[1].strip()
        naziv = cena_i_naslov[0].replace("<h3>","").replace("</h3>\n","")
        assert cena.isnumeric()



# druga_strana = requests.get("http://gresnik.com/jelo.php?jelo=pica")
# print(druga_strana.status_code)