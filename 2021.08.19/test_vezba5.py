import requests
from bs4 import BeautifulSoup


def test_sabiranje():
    for a in range(10): 
        for b in range(10):
            ocekivani_rezultat = str(a + b)
            odgovor = requests.get(f"http://gresnik.com/sabiranje.php?a={a}&b={b}") 
            html = BeautifulSoup(odgovor.text,features='html.parser')
            dobijeni_rezultat = html.find("span").text
            print(a,b,ocekivani_rezultat,dobijeni_rezultat,ocekivani_rezultat==dobijeni_rezultat)
            assert ocekivani_rezultat == dobijeni_rezultat